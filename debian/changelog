doona (1.0+git20190108-2) UNRELEASED; urgency=medium

  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Fri, 17 Jan 2020 19:03:44 +0000

doona (1.0+git20190108-1) unstable; urgency=medium

  * Update to latest changes from upstream tree.
  * Migrate to debhelper-compat and bump to 12.
  * Bump Standards-Version to 4.4.0.
  * Update manpage to match new version.
  * Bump copyright years.

 -- Hugo Lefeuvre <hle@debian.org>  Sat, 03 Aug 2019 13:37:59 +0200

doona (1.0+git20160212-2) unstable; urgency=medium

  * Update Vcs-* fields (salsa migration).
  * Update team maintainer address.
  * Bump compat level to 11.
  * debian/control:
    - Bump Standards-Version to 4.1.3.
    - Depend on debhelper >= 11 instead of >= 10.
    - Set priority to optional (extra deprecated).
  * Bump copyright years.
  * Run wrap-and-sort -a.
  * Remove trailing whitespaces from debian/rules.
  * debian/changelog:
    - Add missing colon in closes from changelog entry 1.0+git20160212-1.

 -- Hugo Lefeuvre <hle@debian.org>  Mon, 26 Mar 2018 11:33:21 +0200

doona (1.0+git20160212-1) unstable; urgency=medium

  * Team upload.

  [ Sophie Brun ]
  * Import new upstream release
  * Add a patch to work with Perl 5.22.2-5 (closes: #838630)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 27 Sep 2016 14:46:04 +0200

doona (0.7+git20131211-1) unstable; urgency=low

  * Initial release (Closes: #832910)

 -- Hugo Lefeuvre <hle@debian.org>  Fri, 29 Jul 2016 14:35:29 +0200
